export const LANG_ES_NAME = 'es';

export const LANG_ES_TRANS = {

    // CATEGORIAS
    'Cleaner': 'Limpiador',
    'Personal Trainer': 'Entrenador Personal',
    'Driver': 'Chofer',
    'Waiter': 'Camarero',
    'Massage Therapist': 'Fisioterapeuta',

    // registry.page
    'Sign Up': 'Crear Cuenta',
    'Log In': 'Iniciar Sesión',

    // signup.page
    'On Sign up accept Terms and Conditions of Use and the Politic and Privacy of Plapper': 'Al registrarte aceptas los Términos y Condiciones de Uso y la Política de Privacidad de Plapper',

    // first-step.component
    '3 steps and you are in!': '¡3 pasos y estás dentro!',
    'User Name': 'Nombre de Usuario',
    'Repeat Password': 'Repite la Contraseña',
    'Continue': 'Continuar',
    'You can Sign up too with': 'También puedes registrarte con',

    // second-step.component
    'Your personal info': 'Tu información personal',
    'Name': 'Nomber',
    'Surname': 'Primer Apellido',
    'Second Surname': 'Segundo Apellido',
    'Sex': 'Sexo',
    'Male': 'Hombre',
    'Female': 'Mujer',
    'Birth Date': 'Fecha de Nacimiento',
    'Previous': 'Anterior',

    // third-step.component
    'Your Address': 'Tu dirección',
    'Comunity': 'Comunidad',
    'Province': 'Provincia',
    'Village': 'Ciudad',
    'Address': 'Calle',
    'Number': 'Número',
    'Letter': 'Letra',
    'Finish': 'Finalizar',

    // login.page
    'Hi again!': '¡Hola de nuevo!',
    'e-mail': 'e-mail',
    'Password': 'contraseña',
    'Enter': 'Entrar',
    'You can log in too with': 'También puedes iniciar sesión con',
    'Have you forgot your password?' : '¿Has olvidado tu contraseña?',

    // service-list.page
    'Categories': 'Categorías',
    'What do you need?': '¿Que necesitas?',

    // lateral-menu.page
    'Personal Info': 'Info. Personal',
    'My Services': 'Mis Servicios',
    'Log Out': 'Cerrar Sesión',

    // service-list.page
    'Search on': 'Buscar en',
};