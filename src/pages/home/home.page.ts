import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WorkersListPage } from '../workers-list/workers-list.page';
import { HttpService } from './../shared/services/httpService.service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.page.html'
})
export class HomePage {
  public searchInput = '';
  public username = '';

  public categories = [];

  constructor(private nav: NavController,
              private http: HttpService) {

    this.http.get('/services/categories').subscribe((data)=> {
        this.categories = data;
    });
  }

  public onCancel() {
    this.searchInput = '';
  }

  public selectCategory(category) {
    this.nav.push(WorkersListPage, { category: category });
  }
}