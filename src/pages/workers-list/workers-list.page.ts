import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { HttpService } from './../shared/services/httpService.service';

@Component({
    selector: 'workers-list-page',
    templateUrl: 'workers-list.page.html'
})

export class WorkersListPage {
    @ViewChild(Slides) slides: Slides;
    public searchInput = '';
    public category: any;

    private workers: Array<Object>;

    public filters = [
        { name: 'Recomended', show: true },
        { name: 'Discover', show: false },
        { name: 'Favorites', show: false }
    ];

    constructor(public nav: NavController, 
                public params: NavParams,
                private http: HttpService) {

        this.category = params.get("category");

        this.http.get('/workers/workers?categoryId='+this.category.id).subscribe((data)=> {
            this.workers = data;
        });
    }

    private _updateFilterSelected(index) {
        for(let i=0; i<this.filters.length; i++) {
            this.filters[i].show = ((i == index) ? true : false);
        }
    }

    public setFilterSelected(index) {
        this.slides.slideTo(index, 500);
        this._updateFilterSelected(index);
    }

    public slideChanged() {
        let currentIndex = this.slides.getActiveIndex();
        if(currentIndex >= this.filters.length) { currentIndex = this.filters.length-1; }
        this._updateFilterSelected(currentIndex);
    }

}