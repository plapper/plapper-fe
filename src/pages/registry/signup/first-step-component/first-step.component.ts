import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from './../../../shared/services/httpService.service'
import { Validations } from './../../../shared/classes/validations';

@Component({
  selector: 'first-step',
  templateUrl: './first-step.component.html',
})
export class FirstStepComponent implements OnInit {
    @Output() nextStep = new EventEmitter<Object>();

    public facebookImage = 'assets/icons/facebook.png';
    public googleImage = 'assets/icons/google.png';

    public userName        = '';
    public email           = '';
    public password        = '';
    public repeatPassword  = '';

    public validations: Validations;

    public userNameValidation         = '';
    public emailValidation            = '';
    public passwordValidation         = '';
    public repeatPasswordValidation   = '';

    public userNameBound: number       = 0;
    public emailBound: number          = 0;
    public passwordBound: number       = 0;
    public repeatPasswordBound: number = 0;

    constructor(private http: HttpService) {
        this.validations = new Validations();
    }

    ngOnInit() {}



    public validateUserName() {
        this.userNameValidation = this.validations.userName(this.userName);
        if( this.userNameValidation == '') {
            this.http.get('/registry/check/userName?userName='+this.userName).subscribe((data)=> {
                if(data.exist) {
                    console.log("USERNAME exist!");
                } else {
                    console.log("USERNAME OK!");
                }
            });
        }
    }

    public validateEmail() {
        this.emailValidation = this.validations.email(this.email);
        if(this.emailValidation == '') {
            this.http.get('/registry/check/email?email='+this.email).subscribe((data)=> {
                if(data.exist) {
                    console.log("EMAIL exist!");
                } else {
                    console.log("EMAIL OK!");
                }
            });
        }
    }

    public validatePassword() {
        this.passwordValidation = this.validations.password(this.password);
        if(this.passwordValidation == '') {
            console.log("PASSWORD OK");
        }
    }

    public validateRepeatPassword() {
        this.repeatPasswordValidation = this.validations.repeatPassword(this.password, this.repeatPassword);
        if(this.repeatPasswordValidation == '') {
            console.log("Password matching OK.");
        }
    }

    public goToNextStep() {
        if(this.userNameValidation == 'NO-ERROR' && this.emailValidation == 'NO-ERROR' &&
            this.passwordValidation == 'NO-ERROR' && this.repeatPasswordValidation == 'NO-ERROR') {

            this.nextStep.emit({ userName: this.userName, email: this.email, password: this.password });

        } else {
            if(this.userNameValidation != 'NO-ERROR') { this.userNameBound = 1; }
            if(this.emailValidation != 'NO-ERROR') { this.emailBound = 1; }
            if(this.passwordValidation != 'NO-ERROR') { this.passwordBound = 1; }
            if(this.repeatPasswordValidation != 'NO-ERROR') { this.repeatPasswordBound = 1; }
        }
    }
}