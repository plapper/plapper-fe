import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpService } from './../../../shared/services/httpService.service';

@Component({
  selector: 'third-step',
  templateUrl: './third-step.component.html'
})
export class ThirdStepComponent implements OnInit {
    @Output() nextStep = new EventEmitter<Object>();

    public comunities:  any;
    public provinces:   any;
    public villages:    any;

    private comunity:   number = undefined;
    private province:   number = undefined;
    private village:    number = undefined;
    private address     = '';
    private number      = '';
    private letter      = '';

    constructor(private http: HttpService) {

        this.http.get('/registry/comunities').subscribe((data)=> {
            this.comunities = data;
        });
    }

    ngOnInit() {}

    public finishSteps() {
        this.nextStep.emit({ comunity:  this.comunity,
                             province:  this.province,
                             village:   this.village,
                             address:   this.address,
                             number:    this.number,
                             letter:    this.letter
        });
    }


    public comunityChange(id) {
        if(id === undefined) { return; }

        this.http.get('/registry/provinces?comunity_id='+id).subscribe((data)=> {
            this.provinces  = data;
            this.province   = undefined;
            this.village    = undefined;
        });
    }

    public provinceChange(id) {
        if(id === undefined) { return; }

        this.http.get('/registry/villages?province_id='+id).subscribe((data)=> {
            this.villages   = data;
            this.village    = undefined;
        });
    }


    public goToPrevStep() {
        this.nextStep.emit(null);
    }
}