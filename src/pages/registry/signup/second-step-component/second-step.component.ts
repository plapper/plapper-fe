import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'second-step',
  templateUrl: './second-step.component.html'
})
export class SecondStepComponent implements OnInit {
    @Output() nextStep = new EventEmitter<Object>();

    public base64Image: string;

    public name            = '';
    public surname         = '';
    public secondSurname   = '';
    public sex             = undefined;
    public birthDate       = '';

    public sexs = [
        { id: 0, name: 'Male'},
        { id: 1, name: 'Female'}
    ];

    constructor() {}

    ngOnInit() {}

    public goToNextStep() {
        this.nextStep.emit({ name:          this.name,
                             surname:       this.surname,
                             secondSurname: this.secondSurname,
                             birthDate:     this.birthDate
                        });
    }

    public goToPrevStep() {
        this.nextStep.emit(null);
    }
}