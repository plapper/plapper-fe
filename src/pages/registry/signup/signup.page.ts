import { Component, OnInit, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpService } from './../../shared/services/httpService.service'
import { HomePage } from './../../home/home.page';

@Component({
  selector: 'signup-page',
  templateUrl: './signup.page.html',
})
export class SignupPage implements OnInit {
  //public backgroundImage = 'assets/backgrounds/registry.png';

  private data: any;

  public nextStep = new EventEmitter();
  private step = 0;
  public steps = ['User Info', 'Personal Info', 'Address Info'];

  constructor(public nav: NavController,
              private http: HttpService) {
      this.data = {
        user: {},
        personal: {},
        address: {}
      };
  }

  ngOnInit() {
  }

  // Handle steps data to create user. On last step move to user created page.
  public handleNextStep(data) {

    if (data === null) {
        this.step--;
        return;
    }

    switch (this.step) {
        case 0:
            this.data.user = data;
            break;

        case 1:
            this.data.personal = data;
            break;

        case 2:
            this.data.address = data;
            break;
    }

    this.step++;
    if (this.step === 3) { this.createUser(); }
  }



  private createUser() {
    console.log(this.data);

    this.http.post('/registry/signup', {data: this.data}).subscribe((data)=> {
    });

    this.nav.setRoot(HomePage);
  }

  public goToRegistryPage() {
    this.nav.pop();
  }
}