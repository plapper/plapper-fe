import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from './login/login.page';
import { SignupPage } from './signup/signup.page';

@Component({
  selector: 'registry-page',
  templateUrl: './registry.page.html'
})
export class RegistryPage {
  public extraOptions = { pager: true };
  public slides = [
    { img: 'assets/backgrounds/welcome1.jpg' },
    { img: 'assets/backgrounds/welcome2.jpg' },
    { img: 'assets/backgrounds/welcome3.jpg' },
    { img: 'assets/backgrounds/welcome4.jpg' },
    { img: 'assets/backgrounds/welcome5.jpg' }
  ];

  constructor(private nav: NavController) {}

  public goToLoginPage() {
    this.nav.push(LoginPage);
  }

  public goToSignupPage() {
    this.nav.push(SignupPage);
  }

  ngOnInit() {}
}