import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from './../../home/home.page';
import { HttpService } from './../../shared/services/httpService.service'
import { Validations } from './../../shared/classes/validations';

@Component({
  selector: 'login-page',
  templateUrl: './login.page.html',
})
export class LoginPage implements OnInit {
  public backgroundImage = 'assets/backgrounds/registry.png';
  public facebookImage = 'assets/icons/facebook.png';
  public googleImage = 'assets/icons/google.png';

  public email = '';
  public password = '';

  public emailValidation         = '';
  public passwordValidation      = '';

  public emailBound: number      = 0;
  public passwordBound: number   = 0;

  private _validations: Validations;

  constructor(public nav: NavController,
              private http: HttpService) {

    this._validations = new Validations();
  }

  ngOnInit() {}

  private _checkData(data) {
    if(!data.email) {
      this.emailValidation = 'Email is not associated to any user.'
      this.emailBound = 1;
    } else if(!data.password) {
      this.passwordValidation = 'Password incorrect for this user.';
      this.passwordBound = 1;
    }
  }

  public validateEmail() {
        this.emailValidation = this._validations.email(this.email);
        if(this.emailValidation == '') {
          console.log("USERNAME OK");
        }
    }

  public validatePassword() {
      this.passwordValidation = this._validations.password(this.password);
      if(this.passwordValidation == '') {
          console.log("PASSWORD OK");
      }
  }

  public login() {
    if(this.emailValidation == 'NO-ERROR' && this.passwordValidation == 'NO-ERROR') {
      let data = { email: this.email, password: this.password }
      this.http.post('/registry/login', {data: data}).subscribe((data)=> {
        if(data.err) {
          this._checkData(data);
        } else {
          console.log("TOKEN: " + data.token);
          this.http.setAuthenticateToken(data.token);
          this.nav.setRoot(HomePage);
        }
      });

    } else {
      if(this.emailValidation != 'NO-ERROR') { this.emailBound = 1; }
      if(this.passwordValidation != 'NO-ERROR') { this.passwordBound = 1; }
    }
    
  }

  public authenticate() {
    
  }
}
