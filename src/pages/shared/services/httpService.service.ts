import { Http, Response, Headers, RequestOptions } from '@angular/http'
import {Observable} from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Config } from './../../../app/config';


@Injectable()
export class HttpService {
    private headers: Headers;
    private options: RequestOptions;
    private backend: string;

    constructor(private http: Http, private config: Config) {
        this.headers    = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        this.options    = new RequestOptions({ headers: this.headers }); // Create a request option
        this.backend    = 'http://' + config.backend.ip + ':' + config.backend.port;
        //this.backend    = 'http:///plapper.freedynamicdns.net:8888/api';
    }

    public get(path): Observable<any> {
        let url = this._getUrl(path);

        return this.http.get(url, this.options)
            .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }

    public post(path, data): Observable<any> {
        let url = this._getUrl(path);

        return this.http.post(url, data, this.options)
            .map((res:Response) => res.json()) // ...and calling .json() on the response to return data
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')); //...errors if any
    }


    public setAuthenticateToken(token) {
        this.headers.append('Authorization', token);
        this.options    = new RequestOptions({ headers: this.headers });
    }

    private _getUrl(path: string) {
        return (path.indexOf('registry') >= 0) ? this.backend+path : this.backend+'/api'+path;
    }

}