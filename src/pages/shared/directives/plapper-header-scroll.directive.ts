import { Directive, HostListener } from '@angular/core';
import { AppService } from '../../../app/app.service';

@Directive({ selector: '[plapper-header-scroll]' })
export class PlapperHeaderScrollDirective {

    constructor(private appService: AppService) {}

    @HostListener("ionScroll", ['$event'])
    checkScroll($event) {
        this.appService.scroll.next(event.target);
    }
}