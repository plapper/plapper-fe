import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[plapper-void-button]' })
export class PlapperVoidButtonDirective {

    constructor(el: ElementRef) {
       el.nativeElement.style.background = 'rgba(255,255,255,0)';
       el.nativeElement.style.textAlign = 'center';
    }
}