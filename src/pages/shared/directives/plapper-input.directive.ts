import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[plapper-input]' })
export class PlapperInputDirective {

    constructor(el: ElementRef) {
       el.nativeElement.style.display = 'block';
       el.nativeElement.style.margin = 0;
       el.nativeElement.style.appearance = 'none';
       el.nativeElement.style.family = 'sans-serif';
       el.nativeElement.style.color = '#333333';
       el.nativeElement.style.textAlign = 'center';
       el.nativeElement.style.width = '100%';
       el.nativeElement.style.shadow = 'none';
       el.nativeElement.style.borderWidth = 0;
       el.nativeElement.style.borderBottom = 'solid #777777 2px';
       el.nativeElement.style.fontWeight = '600';
       
    }
}