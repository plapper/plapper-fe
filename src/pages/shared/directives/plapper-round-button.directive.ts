import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[plapper-round-button]' })
export class PlapperRoundButtonDirective {

    constructor(el: ElementRef) {
       el.nativeElement.style.display = 'block';
       el.nativeElement.style.background = 'blue';
       el.nativeElement.style.color = 'white';
       el.nativeElement.style.borderRadius = '50%';
    }
}