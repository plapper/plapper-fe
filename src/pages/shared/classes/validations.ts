export class Validations {

    private _noError: string;
    private _emptyMessage: string;
    private _maxLengthMessage: string;
    private _minLengthMessage: string;
    private _invalidChars: string;
    private _emailInvalidFormat: string;
    private _notMatchPasswords: string;
    private _whitespaceMessage: string;

    constructor() {
        this._noError               = "NO-ERROR";
        this._emptyMessage          = "This field can't be empty.";
        this._maxLengthMessage      = "Max Length of field: ";
        this._minLengthMessage      = "Min Length of field: ";
        this._invalidChars          = "Type only letters, numbers and this chars [. _ - /]";
        this._emailInvalidFormat    = "Email address with Invalid format.";
        this._notMatchPasswords     = "Passwords doesnt match.";
        this._whitespaceMessage     = "Field cant has whitespaces.";
    }

    private _hasWhitespace(str: string): boolean {
        return ((str.indexOf(' ') >= 0) ? true : false);
    }

    private _matchStrings(str1: string, str2: string): boolean {
        return ((str1 === str2) ? true : false);
    }

    private _isMinLength(str: string, ml: number): boolean {
        return ((str.length < ml) ? true : false);
    }

    private _isMaxLength(str: string, ml: number): boolean {
        return ((str.length > ml) ? true : false);
    }

    /*
    private _onlyLettersAndDigits(str: string): boolean {
      return ((str.match(/^[a-zA-Z0-9]+$/)) ? true : false);
    }
    */

    private _onlyLettersAndDigitsAndSpecialChars(str: string): boolean {
        return ((str.match(/^[ A-Za-z0-9_.-/]*$/)) ? true : false);
    }

    private _emailFormat(str: string): boolean {
        let regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return ((str.match(regex)) ? true : false);
    }


    /* PUBLIC METHODS */
    public userName(userName: string): string {
        if(userName === "") { return this._emptyMessage; }
        if(this._isMinLength(userName, 6)) { return this._minLengthMessage+"6"; }
        if(this._isMaxLength(userName, 20)) { return this._maxLengthMessage+"20"; }
        if(this._hasWhitespace(userName)) { return this._whitespaceMessage; }
        if(!this._onlyLettersAndDigitsAndSpecialChars(userName)) { return this._invalidChars; }
        
        return this._noError;
    }

    public email(email: string): string {
        if(email === "") { return this._emptyMessage; }
        if(!this._emailFormat(email)) { return this._emailInvalidFormat; }

        return this._noError;
    }

    public password(password: string): string {
        if(password === "") { return this._emptyMessage; }
        if(this._isMinLength(password, 6)) { return this._minLengthMessage+"6"; }
        if(this._isMaxLength(password, 20)) { return this._maxLengthMessage+"20"; }
        if(!this._onlyLettersAndDigitsAndSpecialChars(password)) { return this._invalidChars; }

        return this._noError;
    }

    public repeatPassword(password: string, repeatPassword: string): string {
        if(repeatPassword === "") { return this._emptyMessage; }
        if(!this._matchStrings(password, repeatPassword)) { return this._notMatchPasswords; }

        return this._noError;
    }   
}