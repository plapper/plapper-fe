import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fillArray'
})
export class FillArrayPipe implements PipeTransform {
  transform(value) {
    return (new Array(5)).fill(Math.trunc(value));
  }
}