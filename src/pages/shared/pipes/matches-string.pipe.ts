import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'matchesString'
})
export class MatchesStringPipe implements PipeTransform {
    transform(items: Array<any>, itm: string): Array<any> {
        let filter = itm.toLocaleLowerCase();
        return filter ? items.filter(item => item.name.toLocaleLowerCase().indexOf(filter) != -1)
                        : items;
    }
}