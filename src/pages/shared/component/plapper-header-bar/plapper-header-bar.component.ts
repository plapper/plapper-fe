import { Component, Input, ViewChild, ElementRef, Renderer } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { Subscription} from 'rxjs/Rx';
import { AppService } from '../../../../app/app.service';

@Component({
  selector: 'plapper-header-bar',
  templateUrl: 'plapper-header-bar.component.html'
})
export class PlapperHeaderBarComponent {
    @Input() title: string;
    @Input() leftButton: string;

    @ViewChild('plapperHeader') plapperHeader: ElementRef;

    public menuButton = 'assets/icons/menu.png';
    public backButton = 'assets/icons/flecha_izq.png';

    public headerStyle = 'header-div';
    public scrolled = false;
    private scrollSubs: Subscription;

    constructor(public nav: NavController,
                public menuCtrl: MenuController,
                public appService: AppService,
                public renderer: Renderer) {

        // Control float style when init scroll to down.
        this.scrollSubs = this.appService.scroll.subscribe((event)=> {
            if(event.scrollTop >= 16) {
                this.renderer.setElementClass(this.plapperHeader.nativeElement, 'header-float-div', true);
            } else {
                this.renderer.setElementClass(this.plapperHeader.nativeElement, 'header-float-div', false);
            }
        });
    }

    ngDestroy() {
        this.scrollSubs.unsubscribe();
    }

    public isButtonEnabled(button) {
        if(this.leftButton == button) { return true; }
        return false;
    }

    public openMenu() {
        this.menuCtrl.open();
    }

    public goBack() {
        this.nav.pop();
    }
}