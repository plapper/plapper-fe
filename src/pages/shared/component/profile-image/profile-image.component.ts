import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'profile-image',
  templateUrl: 'profile-image.component.html'
})
export class ProfileImageComponent {
    //public userImage = 'assets/icon/profile-pictures.png';
    public userImage = 'assets/icon/profile.jpg';
    //public userImage = null;

    public cameraIcon = 'assets/icons/camara_perfil.png';

  constructor(private nav: NavController) {
  }
}