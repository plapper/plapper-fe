import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
  selector: 'plapper-search-input',
  templateUrl: 'plapper-search-input.component.html'
})
export class PlapperSearchInputComponent {
    @Input() model: string;
    @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
    
    @Input() placeholder: string;

    @ViewChild('searchInput') searchInput; 

    public searchImage = 'assets/icons/buscador.png';
    public cleanImage = 'assets/icons/cerrar.png';

    constructor() {}

    doCheck() {
        this.modelChange.next(this.model);
    }

    cleanInput() {
        this.model = '';
        this.modelChange.next(this.model);
    }

    selectInput() {
        this.searchInput.nativeElement.focus();
    }
}