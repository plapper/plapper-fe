import { Component, Input, OnChanges, Renderer, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'plapper-icon',
  templateUrl: 'plapper-icon.component.html'
})
export class PlapperIconComponent implements OnChanges {
  @Input() show?: boolean;

  @ViewChild('plapperIcon') plapperIcon: ElementRef;

  constructor(private renderer: Renderer) {

  }

  ngOnChanges() {
    this._changeStyle();
  }

  private _changeStyle() {
    if(this.show) {
      this.renderer.setElementClass(this.plapperIcon.nativeElement, "hide-icon", false);
      this.renderer.setElementClass(this.plapperIcon.nativeElement, "show-icon", true);
    } else {
      this.renderer.setElementClass(this.plapperIcon.nativeElement, "show-icon", false);
      this.renderer.setElementClass(this.plapperIcon.nativeElement, "hide-icon", true);
    }
  }

}