import { Component, Input, Output, EventEmitter, ViewChild, OnChanges, ElementRef, Renderer } from '@angular/core';

@Component({
  selector: 'plapper-input',
  templateUrl: 'plapper-input.component.html'
})
export class PlapperInputComponent implements OnChanges {
    @Input() model: string;
    @Output() modelChange: EventEmitter<string> = new EventEmitter<string>();
    
    @Input() error?: string;
    @Output() errorChange: EventEmitter<string> = new EventEmitter<string>();

    @Input() bound?: number;
    @Output() boundChange: EventEmitter<number> = new EventEmitter<number>();

    @Input() type: string;
    @Input() placeholder: string;
    @Output() blur: EventEmitter<Event> = new EventEmitter<Event>();

    @ViewChild('input') plapperInput: ElementRef;
    @ViewChild('borderBottom') borderBottom: ElementRef;

    private _auxModel: string;
    private _onBlur: boolean;

    constructor(public renderer: Renderer) {
        this._onBlur = false;
    }

    ngOnChanges() {
        if(this._onBlur) { this._checkErrors(); }
        if(this.bound == 1) { 
            this._checkErrors();
            this._boundInput();
        }
    }

    private _removeClasses() {
        this.renderer.setElementClass(this.plapperInput.nativeElement, 'input-error', false);
        this.renderer.setElementClass(this.plapperInput.nativeElement, 'input-ok', false);
        this.renderer.setElementClass(this.borderBottom.nativeElement, 'border-bottom-error', false);
        this.renderer.setElementClass(this.borderBottom.nativeElement, 'border-bottom-ok', false);
        this.plapperInput.nativeElement.type = this.type;
    }

    private _boundInput() {
        this.renderer.setElementClass(this.plapperInput.nativeElement, 'bound-elem', true);
        this.renderer.setElementClass(this.borderBottom.nativeElement, 'bound-elem', true);
        this.renderer.listen(this.borderBottom.nativeElement, "animationend", ()=> {
            this.bound = 0;
            this.boundChange.emit(this.bound);
            this.renderer.setElementClass(this.plapperInput.nativeElement, 'bound-elem', false);
            this.renderer.setElementClass(this.borderBottom.nativeElement, 'bound-elem', false);
        });
    }

    private _checkErrors() {
        this._removeClasses();
        if(this.error == this.model) {this.model = this._auxModel; }
        this._auxModel = this.model;
        if(this.error !== '' && this.error !== 'NO-ERROR' && this.error !== undefined) {
            this.renderer.setElementClass(this.plapperInput.nativeElement, 'input-error', true);
            this.renderer.setElementClass(this.borderBottom.nativeElement, 'border-bottom-error', true);
            this.plapperInput.nativeElement.type = 'text';
            this.model = this.error;
        } else if(this.model !== '') {
            this.renderer.setElementClass(this.plapperInput.nativeElement, 'input-ok', true);
            this.renderer.setElementClass(this.borderBottom.nativeElement, 'border-bottom-ok', true);
            this.plapperInput.nativeElement.type = this.type;
        }
        this._onBlur = false;
    }


    public onInputChanges() {
        this.modelChange.next(this.model);
    }

    public onClick() {
        if(this.error == '') { return; }
        
        this.errorChange.next('');
        this.model = this._auxModel;
        this._auxModel = '';
        this._removeClasses();
    }

    public onBLur(event) {
        this._onBlur = true;
        this.blur.emit(event);
    }
}