import { Component, Input } from '@angular/core';

@Component({
    selector: 'plapper-workers',
    templateUrl: 'plapper-workers.component.html'
})

export class PlapperWorkersComponent {
    @Input() searchInput: string;
    @Input() workers: Array<Object>;

    public starImage = 'assets/icons/star_on.png';
    public grayStarImage = 'assets/icons/star_off.png';
    public commentaryIcon = 'assets/icons/comentarios.png';

    //public model: Array<any>;

    constructor() {
    /*    this.model = [
                { name: 'Taberna La Gula', stars: 5, openYear: 2005 },
                { name: 'Restaurante la Panocha', stars: 2, openYear: 2004 },
                { name: 'Rest. Don Pablo', stars: 1, openYear: 2012 },
                { name: 'Taberna La Gula', stars: 5, openYear: 2005 },
                { name: 'Restaurante la Panocha', stars: 2, openYear: 2004 },
                { name: 'Rest. Don Pablo', stars: 1, openYear: 2012 },
                { name: 'Tapas Variadas', stars: 4, openYear: 2003 },
                { name: 'Tapas Variadas', stars: 4, openYear: 2003 }
            ];*/
    }
}