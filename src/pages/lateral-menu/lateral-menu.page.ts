import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RegistryPage } from '../registry/registry.page';

@Component({
  selector: 'page-lateral-menu',
  templateUrl: 'lateral-menu.page.html'
})
export class LateralMenuPage {
  public backButton = 'assets/icons/flecha_izq.png';

  public personalInfoIcon = 'assets/icons/editar_perfil.png';
  public myServicesIcon = 'assets/icons/mis_servicios.png';

  constructor(private nav: NavController) {}

  public disconnectUser() {
    this.nav.setRoot(RegistryPage);
  }
}