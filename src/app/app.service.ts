import {Observable, Subject} from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class AppService {
    public scroll   = new Subject<any>();
    public $scroll  = new Observable(scroll);

    constructor() {}
}