import { Component } from '@angular/core';

import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { TranslateService } from '../translate/translate.service';

import { RegistryPage } from './../pages/registry/registry.page';
import { HomePage } from '../pages/home/home.page';

@Component({
  templateUrl: 'app.html',
})
export class AppComponent {
  private rootPage;
  private logged = false;

  public translatedText: string;
  public supportedLangs: any[];

  constructor( platform: Platform,
               statusBar: StatusBar,
               splashScreen: SplashScreen,
               keyboard: Keyboard,
               private _translate: TranslateService) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      keyboard.disableScroll(true);
      statusBar.styleDefault();
      splashScreen.hide();
    });


    if(!this.logged) {
      this.rootPage = RegistryPage;
    } else {
      this.rootPage = HomePage;
    }
  }

  ngOnInit(){

    this.supportedLangs = [
        { display: 'English', value: 'en' },
        { display: 'Español', value: 'es' }
        ];

        // set current langage
        this.selectLang('en');
  }

  selectLang(lang: string) {
      // set current lang;
      this._translate.use(lang);
  }
}
