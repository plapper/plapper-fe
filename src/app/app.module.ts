import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule} from '@angular/http'

import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AppComponent } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

// CONFIG
import { Config } from './config';

// TRANSLATIONS
import { TRANSLATION_PROVIDERS, TranslatePipe, TranslateService } from '../translate';

// MAIN APP
import { HomePage } from '../pages/home/home.page';

// LATERAL MENU
import { LateralMenuPage } from '../pages/lateral-menu/lateral-menu.page';

// WORKERS
import { WorkersListPage } from '../pages/workers-list/workers-list.page';

// IONIC NATIVE PLUGINS
import { Camera } from '@ionic-native/camera';

// REGISTRY COMPONENTS
import { RegistryPage } from '../pages/registry/registry.page';
import { LoginPage } from '../pages/registry/login/login.page';
import { SignupPage} from '../pages/registry/signup/signup.page';
import { FirstStepComponent } from '../pages/registry/signup/first-step-component/first-step.component';
import { SecondStepComponent } from '../pages/registry/signup/second-step-component/second-step.component';
import { ThirdStepComponent } from '../pages/registry/signup/third-step-component/third-step.component';

// PIPES
import { MatchesStringPipe } from '../pages/shared/pipes/matches-string.pipe';
import { FillArrayPipe } from '../pages/shared/pipes/fill-array.pipe';

// ATTRIBUTE DIRECTIVES
import { PlapperInputDirective } from '../pages/shared/directives/plapper-input.directive';
import { PlapperRoundButtonDirective } from '../pages/shared/directives/plapper-round-button.directive';
import { PlapperVoidButtonDirective } from '../pages/shared/directives/plapper-void-button.directive';

// FUNCTION DIRECTIVES
import { PlapperHeaderScrollDirective } from '../pages/shared/directives/plapper-header-scroll.directive';

// COMPONENTS
import { ProfileImageComponent } from '../pages/shared/component/profile-image/profile-image.component';
import { PlapperSearchInputComponent } from '../pages/shared/component/plapper-search-input/plapper-search-input.component';
import { PlapperHeaderBarComponent } from '../pages/shared/component/plapper-header-bar/plapper-header-bar.component';
import { PlapperWorkersComponent } from '../pages/shared/component/plapper-workers/plapper-workers.component';
import { PlapperIconComponent } from '../pages/shared/component/plapper-icon/plapper-icon.component';
import { PlapperInputComponent } from '../pages/shared/component/plapper-input/plapper-input.component';

// SERVICES
import { HttpService } from '../pages/shared/services/httpService.service';
import { AppService } from '../app/app.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePage,

    WorkersListPage,

    LateralMenuPage,

    RegistryPage,
    LoginPage,
    SignupPage,
    FirstStepComponent,
    SecondStepComponent,
    ThirdStepComponent,

    MatchesStringPipe,
    FillArrayPipe,
    TranslatePipe,

    PlapperInputDirective,
    PlapperRoundButtonDirective,
    PlapperVoidButtonDirective,

    PlapperHeaderScrollDirective,

    ProfileImageComponent,
    PlapperSearchInputComponent,
    PlapperHeaderBarComponent,
    PlapperWorkersComponent,
    PlapperIconComponent,
    PlapperInputComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(AppComponent)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    HomePage,
    WorkersListPage,
    LateralMenuPage,
    RegistryPage,
    LoginPage,
    SignupPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    Camera,
    HttpService,
    AppService,
    TRANSLATION_PROVIDERS, TranslateService, Config,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
